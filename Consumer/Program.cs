﻿using System;
using System.Collections.Generic;

namespace Consumer
{
    class Program
    {
        private static int consumersCount = 10;
        private static int taskPerConsumerCount = 1;
        static void Main(string[] args)
        {
            Console.WriteLine("Running Consumer APP...");
            //consumersCount = ReadNumericLine("Define number of consumers to create: ");
            Console.WriteLine($"Creating {consumersCount} consumers...");
        }

        private List<Models.Consumer> CreateConsumers(int count)
        {
            var consumers = new List<Models.Consumer>();
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var c = new Models.Consumer();
                c.ID = rnd.Next(10000);
                c.Name = Guid.NewGuid().ToString();
            }

            return consumers;
        }

        private static int ReadNumericLine(string text)
        {
            int readValue = 0;
            Console.WriteLine(text);
            string readLine = Console.ReadLine();
            if (!int.TryParse(readLine, out readValue))
            {
                ReadNumericLine(text);
            }

            return readValue;
        }

    }
}
