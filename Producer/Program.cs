﻿using Producer.Models;
using Producer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Producer
{
    class Program
    {
        private static int consumersCount = 5;
        private static int taskPerConsumerCount = 2;
        private static ConsumerRepository consumerRepo;
        private static TaskRepository taskRepo;
        private static TaskExecutionRepository taskExRepo;

        static void Main(string[] args)
        {
            StartProcess().GetAwaiter().GetResult();
        }

        static async System.Threading.Tasks.Task StartProcess()
        {
            consumerRepo = new ConsumerRepository();
            taskRepo = new TaskRepository();
            taskExRepo = new TaskExecutionRepository();

            Console.WriteLine("Running Producer APP...");
            Console.WriteLine($"Get {consumersCount} consumers");
            var consumers = await consumerRepo.GetConsumersWithTasks(consumersCount, taskPerConsumerCount);


            Parallel.ForEach(consumers, consumer =>
            {
                Console.WriteLine($"Got {consumer.Tasks.Count} tasks for {consumer.Name}");
                Parallel.ForEach(consumer.Tasks, async task => {
                    await taskRepo.ProcessTask(task.ID);
                });
            });
        }
    }
}
