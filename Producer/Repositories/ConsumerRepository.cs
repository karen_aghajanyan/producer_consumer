﻿using Microsoft.EntityFrameworkCore;
using Producer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Producer.Repositories
{
    public class ConsumerRepository
    {
        public async void Create(Consumer consumer)
        {
            using (var context = new AppDataContext())
            {
                context.Consumers.Add(consumer);
                await context.SaveChangesAsync();
            }
        }

        public void Update(Consumer consumer)
        {
            using (var context = new AppDataContext())
            {
                var _consumer = context.Consumers.FirstOrDefault(t => t.ID == consumer.ID);
                if (_consumer == null)
                {
                    return;
                }
                _consumer.Name = consumer.Name;
                context.SaveChanges();
            }
        }

        public async void Delete(Consumer consumer)
        {
            using (var context = new AppDataContext())
            {
                var _consumer = await context.Consumers.FirstOrDefaultAsync(t => t.ID == consumer.ID);
                if (_consumer == null)
                {
                    return;
                }
                context.Consumers.Remove(_consumer);
                await context.SaveChangesAsync();
            }
        }

        public async Task<Models.Consumer> Get(int ID)
        {
            using (var context = new AppDataContext())
            {
                var consumer = await context.Consumers.FirstOrDefaultAsync(t => t.ID == ID);
                return consumer;
            }
        }

        public async Task<List<Models.Consumer>> GetAll()
        {
            using (var context = new AppDataContext())
            {
                var consumers = await context.Consumers.ToListAsync();
                return consumers;
            }
        }

        public async Task<List<Consumer>> GetTop(int ConsumersCount)
        {
            using (var context = new AppDataContext())
            {
                var consumers = await context.Consumers.Take(ConsumersCount).ToListAsync();
                return consumers;
            }
        }

        public async Task<List<Consumer>> GetConsumersWithTasks(int ConsumersCount, int TasksCount)
        {
            var taskRepo = new TaskRepository();
            using (var context = new AppDataContext())
            {
                var consumers = await context.Consumers.Take(ConsumersCount).ToListAsync();
                foreach(var consumer in consumers)
                {
                    var tasks = await taskRepo.GetRandomPendingTasks(TasksCount, consumer.ID);
                    consumer.Tasks = new List<Models.Task>();
                    consumer.Tasks.AddRange(tasks);
                }
                return consumers;
            }
        }
    }
}
