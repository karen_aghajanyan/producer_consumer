﻿using Microsoft.EntityFrameworkCore;
using Producer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Producer.Repositories
{
    public class TaskExecutionRepository
    {
        /// <summary>
        /// Calculate average processing time of successfully executed tasks
        /// </summary>
        /// <returns></returns>
        public async Task<double> GetTasksAvgProcessingTime()
        {
            using (var context = new AppDataContext())
            {
                return await context.TaskExecutions
                .Where(t => t.Status == Models.TaskStatus.Done)
                .AverageAsync(t => (t.ExecutionEndDate - t.ExecutionStartDate).TotalMilliseconds);
            }
        }

        /// <summary>
        /// Calculate % of errors
        /// </summary>
        /// <returns></returns>
        public async Task<double> GetPercentOfErrors()
        {
            using (var context = new AppDataContext())
            {
                int failedTaskExecutions = await context.TaskExecutions
               .Where(t => t.Status == Models.TaskStatus.Error)
               .CountAsync();
                int totalTaskExecutions = context.TaskExecutions.Count();
                return (double)(failedTaskExecutions / totalTaskExecutions) * 100;
            }
        }
    }
}
