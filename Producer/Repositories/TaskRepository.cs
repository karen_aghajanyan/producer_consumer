﻿using Microsoft.EntityFrameworkCore;
using Producer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Producer.Repositories
{
    public class TaskRepository
    {
        /// <summary>
        /// Create task
        /// </summary>
        /// <param name="task"></param>
        public async void Create(Models.Task task)
        {
            using(var context = new AppDataContext())
            {
                await context.Tasks.AddAsync(task);
                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Update task
        /// </summary>
        /// <param name="task"></param>
        public async void Update(Models.Task task)
        {
            using (var context = new AppDataContext())
            {
                var _task = await context.Tasks.FirstOrDefaultAsync(t => t.ID == task.ID);
                if (_task == null)
                {
                    return;
                }
                _task.ModificationTime = task.ModificationTime;
                _task.Status = task.Status;
                _task.TaskText = task.TaskText;
                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Delete task
        /// </summary>
        /// <param name="task"></param>
        public async void Delete(Models.Task task)
        {
            using (var context = new AppDataContext())
            {
                var _task = await context.Tasks.FirstOrDefaultAsync(t => t.ID == task.ID);
                if (_task == null)
                {
                    return;
                }
                context.Tasks.Remove(_task);
                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Get task by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public async Task<Models.Task> Get(int ID)
        {
            var task = new Models.Task();
            using (var context = new AppDataContext())
            {
                task = await context.Tasks
                .FirstOrDefaultAsync(t => t.ID == ID);
            }
            return task;
        }

        /// <summary>
        /// Get All tasks
        /// </summary>
        /// <returns></returns>
        public async Task<List<Models.Task>> GetAll()
        {
            var tasks = new List<Models.Task>();
            using (var context = new AppDataContext())
            {
                tasks = await context.Tasks.ToListAsync();
            }
            return tasks;
        }

        /// <summary>
        /// Get tasks by status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<List<Models.Task>> GetAll(Models.TaskStatus status)
        {
            var tasks = new List<Models.Task>();
            using (var context = new AppDataContext())
            {
                tasks = await context.Tasks
                .Where(t => t.Status == status)
                .ToListAsync();
            }
            return tasks;
        }

        public async Task<List<Models.Task>> GetLatestTasks(List<int> ConsumerIDs)
        {
            var tasks = new List<Models.Task>();
            using (var context = new AppDataContext())
            {
                tasks = await context.Tasks
                .Where(t => t.ConsumerID.HasValue && ConsumerIDs.Contains(t.ConsumerID.Value))
                .ToListAsync();
            }
            return tasks;
        }

        public async Task<Models.Task> GetRandomPendingTask()
        {
            var tasks = new List<Models.Task>();
            var r = new Random();
            using (var context = new AppDataContext())
            {
                tasks = await context.Tasks.Where(t => t.Status == Models.TaskStatus.Pending).ToListAsync();
            }
            return tasks.ElementAt(r.Next(0, tasks.Count()));
        }

        public async Task<List<Models.Task>> GetRandomPendingTasks(int Count, int ConsumerID)
        {
            using (var context = new AppDataContext())
            {
                var tasks = await context.Tasks
                        .Where(t => t.Status == Models.TaskStatus.Pending)
                        .OrderBy(arg => Guid.NewGuid())
                        .Take(Count)
                        .ToListAsync();
                foreach(var task in tasks)
                {
                    task.Status = Models.TaskStatus.InProgress;
                    task.ModificationTime = DateTimeOffset.Now;
                    task.ConsumerID = ConsumerID;
                    await context.SaveChangesAsync();
                }
                return tasks;
            }
        }

        public async void TakeTask(int TaskID, int ConsumerID)
        {
            using (var context = new AppDataContext())
            {
                var task = context.Tasks
                .FirstOrDefault(t => t.ID == TaskID);
                task.ConsumerID = ConsumerID;
                task.Status = Models.TaskStatus.InProgress;
                task.ModificationTime = DateTimeOffset.Now;
                await context.SaveChangesAsync();
            }
        }

        public async Task<Models.Task> ProcessTask(int TaskID)
        {
            var task = new Models.Task();

            using (var context = new AppDataContext())
            {
                task = await context.Tasks
                .FirstOrDefaultAsync(t => t.ID == TaskID);

                //Task status is in progress, processing task
                if (task.Status == Models.TaskStatus.InProgress && task.ModificationTime.HasValue)
                {
                    task.Status = Models.TaskStatus.Done;
                    var taskEx = new TaskExecution();
                    taskEx.ExecutionEndDate = DateTimeOffset.Now;
                    taskEx.ExecutionStartDate = task.ModificationTime.Value;
                    taskEx.Status = Models.TaskStatus.Done;
                    taskEx.TaskID = task.ID;
                    await context.TaskExecutions.AddAsync(taskEx);
                }
                else
                {
                    //Task status is not in progress, cannot process task
                    task.Status = Models.TaskStatus.Error;
                    var taskEx = new TaskExecution();
                    taskEx.ExecutionEndDate = DateTimeOffset.Now;
                    taskEx.ExecutionStartDate = task.ModificationTime.HasValue ? task.ModificationTime.Value : DateTimeOffset.Now;
                    taskEx.Status = Models.TaskStatus.Error;
                    taskEx.TaskID = task.ID;
                    await context.TaskExecutions.AddAsync(taskEx);
                }

                task.ModificationTime = DateTimeOffset.Now;
                await context.SaveChangesAsync();
            }
            return task;
        }
    }
}
