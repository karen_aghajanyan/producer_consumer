﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Producer.Models;

namespace Producer.Migrations
{
    [DbContext(typeof(AppDataContext))]
    [Migration("20210407071006_initial1.0.1")]
    partial class initial101
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.5")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Producer.Models.Task", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("ConsumerID")
                        .HasColumnType("int");

                    b.Property<DateTimeOffset>("CreationTime")
                        .HasColumnType("datetimeoffset");

                    b.Property<DateTimeOffset?>("ModificationTime")
                        .HasColumnType("datetimeoffset");

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.Property<string>("TaskText")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            ID = 1,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 253, DateTimeKind.Unspecified).AddTicks(1708), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #1"
                        },
                        new
                        {
                            ID = 2,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7706), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #2"
                        },
                        new
                        {
                            ID = 3,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7759), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #3"
                        },
                        new
                        {
                            ID = 4,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7766), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #4"
                        },
                        new
                        {
                            ID = 5,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7769), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #5"
                        },
                        new
                        {
                            ID = 6,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7773), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #6"
                        },
                        new
                        {
                            ID = 7,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7776), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #7"
                        },
                        new
                        {
                            ID = 8,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7779), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #8"
                        },
                        new
                        {
                            ID = 9,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7783), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #9"
                        },
                        new
                        {
                            ID = 10,
                            CreationTime = new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7786), new TimeSpan(0, 4, 0, 0, 0)),
                            Status = 1,
                            TaskText = "Task text #10"
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
