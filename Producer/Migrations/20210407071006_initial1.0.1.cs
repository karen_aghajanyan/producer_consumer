﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Producer.Migrations
{
    public partial class initial101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "ModificationTime",
                table: "Tasks",
                type: "datetimeoffset",
                nullable: true,
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset");

            migrationBuilder.AlterColumn<int>(
                name: "ConsumerID",
                table: "Tasks",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "ID", "ConsumerID", "CreationTime", "ModificationTime", "Status", "TaskText" },
                values: new object[,]
                {
                    { 1, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 253, DateTimeKind.Unspecified).AddTicks(1708), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #1" },
                    { 2, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7706), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #2" },
                    { 3, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7759), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #3" },
                    { 4, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7766), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #4" },
                    { 5, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7769), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #5" },
                    { 6, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7773), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #6" },
                    { 7, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7776), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #7" },
                    { 8, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7779), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #8" },
                    { 9, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7783), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #9" },
                    { 10, null, new DateTimeOffset(new DateTime(2021, 4, 7, 11, 10, 5, 258, DateTimeKind.Unspecified).AddTicks(7786), new TimeSpan(0, 4, 0, 0, 0)), null, 1, "Task text #10" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "ID",
                keyValue: 10);

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "ModificationTime",
                table: "Tasks",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)),
                oldClrType: typeof(DateTimeOffset),
                oldType: "datetimeoffset",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ConsumerID",
                table: "Tasks",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
