﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Producer.Models
{
    public class AppDataContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=WINDOWS-77PKUUI\\SQLSERVER2019;Initial Catalog=ConsumerProducer;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var tasksToSeed = new List<Task>();
            var consumersToSeed = new List<Consumer>();
            for (int i = 1; i < 10; i++)
            {
                string name = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 6);
                consumersToSeed.Add(new Consumer { ID = i, Name = name});
            }
            for (int i = 1; i < 1000; i++)
            {
                tasksToSeed.Add(new Task
                {
                    ID = i,
                    TaskText = $"Task text #{i}",
                    ConsumerID = null,
                    Status = TaskStatus.Pending,
                    CreationTime = DateTime.Now,
                    ModificationTime = null
                });
            }

            modelBuilder.Entity<Task>().HasData(
                tasksToSeed
            );
            modelBuilder.Entity<Consumer>().HasData(
                consumersToSeed
            );
        }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskExecution> TaskExecutions { get; set; }
        public DbSet<Consumer> Consumers { get; set; }
    }
}
