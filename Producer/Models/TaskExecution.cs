﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Producer.Models
{
    public class TaskExecution
    {
        [Key]
        public int ID { get; set; }
        public int TaskID { get; set; }
        public DateTimeOffset ExecutionStartDate { get; set; }
        public DateTimeOffset ExecutionEndDate { get; set; }
        public TaskStatus Status { get; set; }
    }
}
