﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Producer.Models
{
    public class Consumer
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
