﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Producer.Models
{
    public class Task
    {
        [Key]
        public int ID { get; set; }
        public string TaskText { get; set; }
        public int? ConsumerID { get; set; }
        public TaskStatus Status { get; set; }
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset? ModificationTime { get; set; }
        public List<TaskExecution> TaskExecutions { get; set; }
    }

    public enum TaskStatus { Pending = 1, InProgress = 2, Error = 3, Done = 4 }
}
